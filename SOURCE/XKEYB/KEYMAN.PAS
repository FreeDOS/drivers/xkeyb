{$A-,B-,D-,E-,F-,I-,L-,N-,O-,R-,S-,V-}
{$M $800,0,0}
Uses XKeyb_PI,CRT,DOS;

Type   KeyTyp      = Record
                        Stat,Scan : Byte;
                     End;

Var    SaveDS,SaveSS,
       SaveSP      : Word;
       MySS,MySP   : Word;
       HKey        : KeyTyp;
       E,SaveE     : Array[0..5] of Byte;
       Status      : Byte;
       FuncNum     : Byte;
       HotKeyNum   : Byte;
       HotkeyRow   : Byte;
       ScrBuffer   : Array[0..9,0..59] of Word;
       OldX,OldY   : Byte;
       CRTBase     : Word absolute 0:$463;  { base address of the CRT Controller. }

Const  ScrBase     : Word = $B800;
       MonoCRTBase = $3B0;

Function DezByte(B : Byte) : String;
Var    Loop        : Byte;
Begin
   DezByte[0]:=#3;
   For Loop:=3 DownTo 1 Do
   Begin
      DezByte[Loop]:=Char((B Mod 10)+48);
      B:=B Div 10;
   End;
End;

Procedure OpenWindow(X,Y : Byte);           { Opens a 60*10 window at X,Y.}
Var    Start       : Word;
       Loop        : Byte;
Begin
   OldX:=WhereX;
   OldY:=WhereY;
   Start:=Y*160+X*2;                 { X and Y start at 0. }
   For Loop:=0 To 9 Do
   Begin                             { save old Text. }
      Move(Ptr(ScrBase,Start)^,ScrBuffer[Loop],120);
      Inc(Start,160);
   End;
   TextColor(15);                    { white on black. }
   TextBackGround(0);
   Window(X+1,Y+1,X+60,Y+10);
   ClrScr;                           { Clear the window. }
   Start:=Y*160+X*2;
   For Loop:=0 To 9 Do
   Begin                             { Draw frame. }
      Mem[ScrBase:Start+160*Loop]:=222;
      Mem[ScrBase:Start+160*Loop+118]:=221;
   End;
   For Loop:=1 To 58 Do
   Begin
      Mem[ScrBase:Start+2*Loop]:=223;
      Mem[ScrBase:Start+2*Loop+1440]:=220;
   End;
   Window(X+2,Y+2,X+59,Y+9);         { Set output window to a region in frame. }
End;

Procedure CloseWindow(X,Y : Byte);
Var    Start       : Word;
       Loop        : Byte;
Begin
   Start:=Y*160+X*2;                 { X and Y start at 0. }
   For Loop:=0 To 9 Do
   Begin
      Move(ScrBuffer[Loop],Ptr(ScrBase,Start)^,120);
      Inc(Start,160);
   End;
   Window(1,1,80,25);
   GotoXY(OldX,OldY);
End;

Var    VScan,PScan,Ascii,KeyStat,Ebene : Byte;
Procedure NewHot;                           { Set new Hotkey. }
Var    StatBit     : Byte;
Begin
   StatBit:=$80 shr HotkeyRow;                             { deactivate old Hotkey. }
   GetKey(HotKeyNum,E[0],E[1],E[2],E[3],E[4],E[5]);
   E[HotkeyRow]:=SaveE[HotkeyRow];                         { Old mapping. }
   E[5]:=E[5] and (not StatBit) or (SaveE[5] and StatBit); { Old status. }
   SetKey(HotKeyNum,E[0],E[1],E[2],E[3],E[4],E[5]);

   ClrScr;
   Writeln(' Press the new Hotkey.');
   WaitForKey(VScan,PScan,Ascii,KeyStat,Ebene);
   HotKeyNUm:=PScan;
   HotKeyRow:=Ebene;
   GetKey(HotKeyNum,E[0],E[1],E[2],E[3],E[4],E[5]);   { Momentanous mapping of the Hotkey. }
   SaveE:=E;                                { Save. }
   E[HotkeyRow]:=FuncNum;                   { Register XFunction to selected level.}
   E[5]:=E[5] or ($80 shr HotkeyRow);       { Mark level as occupied by XFunction. }
   SetKey(HotKeyNum,E[0],E[1],E[2],E[3],E[4],E[5]);
End;

Procedure ConvertXStr(Var S1,S2 : String);  { Prepare XString for screen output. }
Var    Loop        : Byte;
Begin
   S2:='';
   Loop:=1;
   While Loop<=Length(S1) Do
   Begin
      Case S1[Loop] of
         #0      : Begin                    { Byte = 0 -> Scancode follows. }
                      Inc(Loop);
                      S2:=S2+'\S'+DezByte(Byte(S1[Loop]));
                   End;
         #1..#35 : S2:=S2+'\A'+DezByte(Byte(S1[Loop]));
         '\'     : S2:=S2+'\\';
         Else S2:=S2+S1[Loop];
      End;
      Inc(Loop);
   End;
End;

Procedure Redefine;                         { Remap key. }
Var    S1,S2       : String;
       KeyNum      : Byte;
       KeyRow      : Byte;
       X,Y         : Byte;
Begin
   ClrScr;
   Writeln(' Key         Macro Assigned    (Press the Hotkey to save)');
   Writeln(' --------------------------------------------------------');
   Write(' [');
   Case Ebene of
      1 : Write('SHIFT ');
      2 : Write('CTRL ');
      3 : Write('ALT ');
      4 : Write('CTRL-ALT ');
   End;
   Write(PScan,']=');
   KeyNum:=PScan;
   KeyRow:=Ebene;

   GetKey(KeyNum,E[0],E[1],E[2],E[3],E[4],E[5]); { Read actual key mapping. }

   If Byte(E[5] shl KeyRow)>127 Then        { Key occupied by XStr ? }
      If E[KeyRow]>200 Then                 { Key is occupied by Xfunc. No remapping }
      Begin
         ClrScr;
         Writeln(' This key is allocated to an XFunction.');
         Writeln(' You must remove the XFunction before assigning a macro!');
         WaitForKey(VScan,PScan,Ascii,KeyStat,Ebene);
         Exit;
      End
      Else
      Begin                                 { If necessary, read old XStr. }
         GetXStr(E[KeyRow],S1);
         ConvertXStr(S1,S2);                { Translate string for screen output. }
      End
   Else
   Begin                                    { Key not yet occupied by XStr. }
      If E[KeyRow]>0 Then
      Begin                                 { Key occupied by sign. Get it. }
         S1:=Char(E[KeyRow]);
         ConvertXStr(S1,S2);
      End
      Else
      Begin                                 { Key unused. }
         S1:='';
         S2:='';
      End;
   End;

   X:=WhereX; Y:=WhereY;                    { Remember the start of the string on screen. }
   Write(S2);

   WaitForKey(VScan,PScan,Ascii,KeyStat,Ebene);
   While (PScan<>HKey.Scan) or ((KeyStat and 15)<>(HKey.Stat and 15)) Do
   Begin
      If (Ascii=8) and (PScan<>0) Then      { Backspace removes last key, except if entered by ALT-8. }
      Begin
         If S1[0]>#0 Then                   { Only if still chars in String. }
         Begin
            If S1[ Byte(S1[0])-1 ]=#0 Then  { Second last byte = 0? -> Last byte is ScanCode. }
               Dec(S1[0],2)                 { Delete 2 Bytes. }
            Else
               Dec(S1[0]);                  { Else only one. }
         End;
      End
      Else                                  { No Backspace. }
      Begin
         If Ascii>0 Then S1:=S1+Char(Ascii) { If Ascii-Value, take it, }
                    Else S1:=S1+#0+Char(VScan);  { else the Scancode. }
      End;
      ConvertXStr(S1,S2);                   { Newly create S2. }
      GotoXY(X,Y+1);
      DelLine;               { Clear line beneath start line to make sure no garbage is remaining. }
      GotoXY(X,Y);                          { Back to the start of the string on screen. }
      Write(S2);                            { And output again. }
      ClrEol;                               { Erase the rest of line. }
      WaitForKey(VScan,PScan,Ascii,KeyStat,Ebene); 
   End;

   If Length(S1)>1 Then                     { -> If only one char, no XStr is needed! }
      If (E[5] and ($80 shr KeyRow)) >0 Then
      Begin                                 { Key already occupied by XStr. It will be overwritten. }
         SetXStr(E[KeyRow],S1);
         If Fehler>0 Then
         Begin
            Writeln(#10);
            Writeln(' Error: XString could not be created.');
            Writeln(' Press any key to exit.');
            WaitForKey(VScan,PScan,Ascii,KeyStat,Ebene);
         End;
      End
      Else                                  { Key not occupied by XStr / XFunc. }
      Begin
         SetXStr(LastXStr+1,S1);            { Install XString from new. }
         If Fehler>0 Then
         Begin
            Writeln(#10);
            Writeln(' Error: XString could not be created.');
            Writeln(' Press any key to exit.');
            WaitForKey(VScan,PScan,Ascii,KeyStat,Ebene);
         End;
         E[KeyRow]:=LastXStr;
         E[5]:=E[5] or ($80 shr KeyRow);    { Occupy key by  XString. }
         SetKey(KeyNum,E[0],E[1],E[2],E[3],E[4],E[5]);
      End
   Else                                     { String contains 0 or 1 char -> no XStr needed. }
   Begin
      If (E[5] and ($80 shr KeyRow)) >0 Then     { Eventually erase XStr mapping. }
      Begin
         SetXStr(E[KeyRow],'');
         E[5]:=E[5] and (not ($80 shr KeyRow));
      End;
      E[KeyRow]:=0;
      If Boolean(S1[0]) Then                { If length=1 }
         E[KeyRow]:=Byte(S1[1]);            { Map key to this one char. }
      SetKey(KeyNum,E[0],E[1],E[2],E[3],E[4],E[5]);
   End;
End;

Procedure Action;
Begin
   OpenWindow(10,5);
   Writeln(' Press the key to assign a macro to, or');
   Writeln(' press the Hotkey to assign a new Hotkey.');
   Writeln(' --------------------------------------------------------');
   WaitForKey(VScan,PScan,Ascii,KeyStat,Ebene);
   If (PScan=HKey.Scan) and
      ((KeyStat and 15)=(Hkey.Stat and 15)) Then NewHot
                                            Else Redefine;
   CloseWindow(10,5);
End;

{$F+}
Procedure Hotkey(Key : KeyTyp);             { Hotkey called directly by XKeyb. }
Begin
   Enter;
   HKey:=Key;
   Action;
   Leave;
End;
{$F-}

{$B-,F-,I+,R+,S+,V+}
Begin
   Writeln;
   Writeln('Keymanager 1.0e   (C) 1992-1999 Dietmar H�hmann.');
   Writeln;
   If TestInstalled<$110 Then
   Begin
      Writeln('Error: XKeyb not found or incorrect XKeyb version.');
      Writeln('This program requires XKeyb version 1.1 or higher.');
      Halt(20);
   End;

   FuncNum:=0;                              { function #0 -> search free one. }
   HotKeyNum:=87;                           { F11 }
   HotKeyRow:=0;                            { Normal. No modifier keys. }

   GetKey(HotkeyNum,E[0],E[1],E[2],E[3],E[4],E[5]);
   SaveE:=E;
   SetXFunc(FuncNum,0,@Hotkey);             { Call at next INT16. }
   If Fehler<>0 Then
   Begin
      Writeln('An error occurred while changing the Hotkey.');
      Halt(Fehler);
   End;

   If CRTBase=MonoCRTBase Then              { Hercules Card installed ? }
   Begin
      ScrBase:=$B000;                       { -> Reset base address of screen memory. }
   End;

   E[HotkeyRow]:=FuncNum;                   { Enter number of function at the corresponding level. }
   E[5]:=E[5] or ($80 shr HotkeyRow);       { Key is occupied at HotkeyRow by XStr / XFunc. }
   SetKey(HotkeyNum,E[0],E[1],E[2],E[3],E[4],E[5]);
   SwapVectors;
   Keep(0);
End.
