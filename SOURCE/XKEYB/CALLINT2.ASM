Code     Segment
         Assume Cs:Code,Es:Code,Ds:Code
Public   CallInt

Jump     Proc Far
         Push [Bp+12]
         Push [Bp+10]
         Ret
Jump     Endp

CallInt  Proc Far
         Push Bp
         Mov  Bp,Sp

         Push Es
         Push Ds
         Push Bp

         Les  Bx,[Bp+6]                          ;put all register values onto the stack
         Mov  Cs:Of,Bx
         Mov  Cs:Se,Es
         Mov  Cx,12

Loop1:   JCxZ ExitLoop
         Mov  Ax,Es:[Bx]
         Push Ax
         Add  Bx,2
         Dec  Cx
         Jmp  Loop1

Exitloop:Popf
         Pop  Ax
         Pop  Ax
         Pop  Ax                                 ;then into register
         Pop  Bx
         Pop  Cx
         Pop  Dx
         Pop  Di
         Pop  Si
         Pop  Ds
         Pop  Es
         Pop  Bp

         Pushf
         Cli

         Call Jump

         Pushf
         Push Cs
         Push Cs
         Push Ax
         Push Bx
         Push Cx
         Push Dx
         Push Di
         Push Si
         Push Ds
         Push Es
         Push Bp

         Les  Bx,Cs:DWord Ptr Of
         Mov  Cl,12

Loop2:   Pop  Ax
         Mov  Es:[Bx],Ax
         Add  Bx,2
         Dec  Cl
         Jnz  Loop2

         Pop  Bp
         Pop  Ds                                 ;restore Bp,Es,Ds
         Pop  Es

         Mov  Sp,Bp                              ;end
         Pop  Bp
         Ret  6
Of       Dw 0
Se       Dw 0
CallInt  Endp
Code     Ends
         End
