{$D-,L-}
{ Lists the actual key mapping. }

Uses XKeyb_PI;

Function DezByte(B : Byte) : String;
Var    Loop        : Byte;
Begin
   DezByte[0]:=#3;
   For Loop:=3 DownTo 1 Do
   Begin
      DezByte[Loop]:=Char((B Mod 10)+48);
      B:=B Div 10;
   End;
End;

Var    Loop,B      : Byte;
       ST          : Byte;
       E           : Array[0..4] of Byte;
       S           : String;
       Version     : Word;
       P           : Pointer;
Begin
   Version:=TestInstalled;
   If Version<$110 Then
   Begin
      Writeln('Error: XKeyb not installed or wrong XKeyb version.');
      Writeln('This program requires Xkeyb Version 1.1 or later.');
      Halt(20);
   End;
   Writeln('[KEYS]');
   For Loop:=1 To 100 Do
   Begin
      GetKey(Loop,E[0],E[1],E[2],E[3],E[4],ST);
      Write(Loop:3);
      If Odd(ST) Then Write('S');
      If Odd(ST shr 1) Then Write('N');
      If Odd(ST shr 2) Then Write('C');
      Write(' ');
      For B:=0 To 4 Do
      Begin
         If Byte(St Shl B)>127 Then Write('!',E[B],' ')
         Else If E[B]<36 Then Write('#',E[B],' ')
         Else Write(Char(E[B]),' ');
      End;
      Writeln;
   End;
   Writeln('[XSTRINGS]');
   For Loop:=1 To LastXStr Do
   Begin
      GetXStr(Loop,S);
      If S<>'' Then
      Begin
         Write(Loop,' ');
         For B:=1 To Length(S) Do
         Begin
            Case S[B] of
               #0      : Begin
                            Inc(B);
                            Write('\S',DezByte(Byte(S[B])));
                         End;
               #1..#35 : Write('\A',DezByte(Byte(S[B])));
               '\'     : Write('\\');
               Else Write(S[B]);
            End;
         End;
         Writeln;
      End;
   End;
   If Version>=$120 Then          { Reading of shift and combination table only if >V1.20! }
   Begin
      Writeln('[SHIFTS]');
      P:=ShiftTabAdr;
      For Loop:=0 To 6 Do
         Write(DezByte(Byte(Pointer(LongInt(P)+Loop)^)),' ');   { Ugly to read, isn't it? }
      Writeln;

      Writeln('[COMBI]');
      P:=CombiTabAdr;
      While Byte(P^)>0 Do                   { Read until end of table. }
      Begin
         If Byte(P^)>35 Then Write(Char(P^),' ')
                        Else Write('#',DezByte(Byte(P^)),' ');
         Inc(LongInt(P));
         Loop:=Byte(P^) shl 1;
         While Loop>0 Do                    { Iterate all combinations. }
         Begin
            Inc(LongInt(P));
            If Byte(P^)>35 Then Write(Char(P^),' ')
                           Else Write('#',DezByte(Byte(P^)),' ');
            Dec(Loop);
         End;
         Inc(LongInt(P));
         Writeln;
      End;
   End;
End.
